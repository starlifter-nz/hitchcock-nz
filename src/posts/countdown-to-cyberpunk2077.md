---
title: Countdown to Cyberpunk 2077
date: '2020-11-19'
tags:
  - gaming
---

<div style="background-color: #0c0c0c; padding: 3rem;">
<div data-type="countdown" data-id="2292966" class="tickcounter" style="width: 100%; position: relative; padding-bottom: 25%"><a href="//www.tickcounter.com/countdown/2292966/cyberpunk-2077" title="Cyberpunk 2077">Cyberpunk 2077</a><a href="//www.tickcounter.com/" title="Countdown">Countdown</a></div><script>(function(d, s, id) { var js, pjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//www.tickcounter.com/static/js/loader.js"; pjs.parentNode.insertBefore(js, pjs); }(document, "script", "tickcounter-sdk"));</script>
</div>
